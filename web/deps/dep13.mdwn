[[!meta title="DEP 13: Debian specific homepage field"]]

    Title: Debian specific homepage field
    DEP: 13
    State: DRAFT
    Date: 2013-01-15
    Drivers: Guido Günther <agx@sigxcpu.org>
    URL: http://dep.debian.net/deps/dep13
    Source: http://anonscm.debian.org/viewvc/dep/web/deps/dep13.mdwn
    License: http://www.jclark.com/xml/copying.txt
    Abstract:
        Control file field to link to Debian specific package information

# Motivation
Many Debian Maintainers keep additional information about a package like
debugging instructions, detailed configuration instructions or answers to
frequently occurring questions outside the source package. A common place for
this is the Debian wiki. Examples are the [OpenStack][0] or [Icedove][1]
packages.

While one can add a reference to README.Debian this is often overlooked 
and not easily machine parseable. The aim of this proposal is to add
a location where this reference can be kept and where it is easily parseable
by package managers or sites like [packages.qa.debian.org][2].

# Proposed Format
The proposal is to add a *Debian-Homepage* field to the source package's control
file similar to the already present *Homepage* (field which is 
intended for upstream use):

    The URL of the Debian specific web site for this package. Preferably
    this page would contain information not suitable for README.Debian
    like extensive debugging help or very detailed configuration examples
    and instructions. The content of this field is a simple URL without 
    any surrounding characters such as `<>'.

# Possible Enhancements
Like the *Homepage* field it might make sense to propagate this information
to the binary package control file *DEBIAN/control* and the Debian source
control file *.dsc* to ease processing for package managers.

[0]: http://wiki.debian.org/OpenStack
[1]: http://wiki.debian.org/Icedove 
[2]: http://packages.qa.debian.org

<!--  LocalWords:  Icedove OpenStack http DEP
 -->
